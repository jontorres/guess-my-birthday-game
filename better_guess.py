from random import randint

#prompt the player for their name
name = input("Hello what is your name?")

# I need three seperate variables. Year, month, and the guess
for guess_number in range (1,6):
    month_number = randint(1,12)
    year_number = randint(1924,2004)

    print("Guess :", guess_number, "were you born in",
            month_number, "/", year_number, "?")

    # Now I need a response - input() because we want the person to type their response
    response = input("yes or no?")

    # if yes print "i knew it" and stop running or use exit() to stop running immediately
    # any response other than "yes" will be seen as a no and we'll print() "Drat! Lemme try again!"

    if response == "yes":
        print("I knew it!")
        exit()

    elif guess_number == 5:
        print("I have other things to do, Good bye!")
    else:
        print("Drat! Lemme try again!")
